package com.mag.boxappenginespring

import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.ModelAndView

data class Box(val lenght: Float, val width:Float, val height:Float)

@RestController
class BoxController{
    @RequestMapping("/boxes")
    fun boxes(): List<Box> {
        return listOf(Box(10f, 11f, 8f),
                Box(15f, 12f, 15f))
    }

    fun box(): ModelAndView{
        return ModelAndView("box")
    }
}