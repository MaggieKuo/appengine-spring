package com.mag.boxappenginespring

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer

@SpringBootApplication
class BoxAppengineSpringApplication: SpringBootServletInitializer()

fun main(args: Array<String>) {
    runApplication<BoxAppengineSpringApplication>(*args)
}

